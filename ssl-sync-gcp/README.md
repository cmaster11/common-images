Example:

```
- name: GCP_SSL_CERTIFICATE_NAME_PREFIX
  value: my-cert-abc
- name: GCP_TARGET_SSL_PROXY_NAME
  value: my-target-ssl-proxy-abc

# Mounted SSL certificate path
- name: SSL_CERT_PATH
  value: "/opt/ssl-cert"

- name: HOOK_SSL_CERTIFICATE_CREATED
  value: 'hookCert() { echo "SSL certificate created: $1"; }; hookCert'
- name: HOOK_TARGET_SSL_PROXY_UPDATED
  value: 'hookProxy() { echo "Target SSL proxy $1 updated to use cert $2"; }; hookProxy'
```

Standalone path: `/opt/ssl-sync-gcp/sync.sh`