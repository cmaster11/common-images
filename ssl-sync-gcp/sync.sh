set -Eeumo pipefail

if [[ "${DEBUG_SET_X:-false}" == "true" ]]; then
    set -x
fi

GCP_SSL_CERTIFICATE_NAME_PREFIX="$GCP_SSL_CERTIFICATE_NAME_PREFIX"
GCP_TARGET_SSL_PROXY_NAME="${GCP_TARGET_SSL_PROXY_NAME:-}"
SSL_CERT_PATH="$SSL_CERT_PATH"

LOOP_SLEEP_SECONDS=${LOOP_SLEEP_SECONDS:-60}

if [[ "${DEBUG_TEST_HOOKS:-false}" == "true" ]]; then

    echo "Testing hooks"

    if [[ -n "${HOOK_SSL_CERTIFICATE_CREATED:-}" ]]; then
        eval ${HOOK_SSL_CERTIFICATE_CREATED} "debug-test-hook-new-certificate" || true
    fi

    if [[ -n "${HOOK_SSL_CERTIFICATE_DELETED:-}" ]]; then
        eval ${HOOK_SSL_CERTIFICATE_DELETED} "debug-test-hook-old-certificate" || true
    fi

    if [[ -n "${HOOK_TARGET_SSL_PROXY_UPDATED:-}" ]]; then
        eval ${HOOK_TARGET_SSL_PROXY_UPDATED} "debug-test-hook-ssl-proxy-name" "link/debug-test-hook-new-certificate" || true
    fi

fi

if [[ ! "$GCP_SSL_CERTIFICATE_NAME_PREFIX" =~ ^[[:alnum:]-]+$ ]]; then
    echo 'Bad SSL certificate name prefix, must match regex ^[[:alnum:]-]+$'
    exit 1
fi

sslCertKey="$SSL_CERT_PATH/tls.key"
sslCertCrt="$SSL_CERT_PATH/tls.crt"

check_secret() {
    if [[ ! -f "$sslCertKey" ]]; then
        echo "Missing certificate .key file"
        return 1
    fi
    if [[ ! -f "$sslCertCrt" ]]; then
        echo "Missing certificate .crt file"
        return 1
    fi

    return 0
}

gcp_existing_get_certificate_id_by_name() {
    name="$1"

    local certificateSelfLink=$(gcloud compute ssl-certificates describe "$name" --format json | jq -r '.selfLink')

    if [[ "$certificateSelfLink" == 'null' ]]; then
        # No certificates found
        return 1
    fi

    echo "$certificateSelfLink"
    return 0
}

gcp_existing_get_certificate_names() {
    local certificatesJSON=$(gcloud compute ssl-certificates list \
        --filter="name~^${GCP_SSL_CERTIFICATE_NAME_PREFIX}-[a-z0-9]{16}\$" \
        --format json)

    local count=$(echo "$certificatesJSON" | jq -r '. | length')
    if [[ "$count" -eq 0 ]]; then
        # No certificates found
        return 0
    fi

    echo "$certificatesJSON" | jq -r '.[] | .name'
}

sync_all() {
    if ! check_secret; then
        echo "Skipping certificate sync, bad check secret"
        return 1
    fi

    # Generate hash of current certificate
    local hash=$(sha256sum "$sslCertKey" "$sslCertCrt" | sha256sum | awk '{print $1}' | cut -c 1-16)
    local currentSecretCertificateName="${GCP_SSL_CERTIFICATE_NAME_PREFIX}-${hash}"
    echo "Certificate from secret name: $currentSecretCertificateName"

    local existingSSLCertificateNames=$(gcp_existing_get_certificate_names)
    local certificateFound=false

    while IFS= read -r sslCertificateName; do
        if [[ -z "$sslCertificateName" ]]; then
            continue
        fi

        echo "Found existing certificate $sslCertificateName"
        if [[ "$sslCertificateName" == "$currentSecretCertificateName" ]]; then
            echo "Existing certificate is up-to-date"
            certificateFound=true
            break
        fi
    done <<< "$existingSSLCertificateNames"

    if [[ "$certificateFound" == "false" ]]; then
        # Create a new certificate with new hash
        local newCertificateName="${GCP_SSL_CERTIFICATE_NAME_PREFIX}-${hash}"
        echo "Creating new certificate with name $newCertificateName"

        gcloud compute ssl-certificates create \
            "$newCertificateName" \
            --global \
            --private-key "$sslCertKey" \
            --certificate "$sslCertCrt" \
            --description "Managed by ssl-sync-gcp: $GCP_SSL_CERTIFICATE_NAME_PREFIX" || return 1

        if [[ -n "${HOOK_SSL_CERTIFICATE_CREATED:-}" ]]; then
            eval ${HOOK_SSL_CERTIFICATE_CREATED} "$newCertificateName" || true
        fi

    fi

    # If we specified an SSL proxy to update, do it
    if [[ -n "$GCP_TARGET_SSL_PROXY_NAME" ]]; then

        # Because SSL proxies are critical, check again that the current SSL certificate exists
        sslCertificateSelfLink=$(gcp_existing_get_certificate_id_by_name "$currentSecretCertificateName") || true

        if [[ -n "$sslCertificateSelfLink" ]]; then

            # First of all, check if the SSL proxy is already up-to-date
            currentProxySSLCertificateSelfLink=$(gcloud compute target-ssl-proxies describe \
                "$GCP_TARGET_SSL_PROXY_NAME" --format json | jq -r '.sslCertificates[0]')

            if [[ "$currentProxySSLCertificateSelfLink" == "$sslCertificateSelfLink" ]]; then

                echo "Target SSL proxy is already up-to-date"

            else

                echo "Updating target SSL proxy $GCP_TARGET_SSL_PROXY_NAME to use SSL certificate $currentSecretCertificateName"
                gcloud compute target-ssl-proxies update \
                "$GCP_TARGET_SSL_PROXY_NAME" \
                --ssl-certificates="$currentSecretCertificateName" || return 1

                if [[ -n "${HOOK_TARGET_SSL_PROXY_UPDATED:-}" ]]; then
                    eval ${HOOK_TARGET_SSL_PROXY_UPDATED} "$GCP_TARGET_SSL_PROXY_NAME" "$currentSecretCertificateName" || true
                fi
            fi

        else
            echo "Cannot update SSL proxy because current secret SSL certificate has not been found"

            # We cannot proceed/delete old certs, or we could disrupt the service
            return 1
        fi

    fi

    # Delete any old certificates
    while IFS= read -r sslCertificateName; do
        if [[ -z "$sslCertificateName" ]]; then
            continue
        fi

        if [[ "$sslCertificateName" == "$currentSecretCertificateName" ]]; then
            continue
        fi

        echo "Deleting existing old certificate $sslCertificateName"
        if gcloud compute ssl-certificates delete "$sslCertificateName"; then
            if [[ -n "${HOOK_SSL_CERTIFICATE_DELETED:-}" ]]; then
                eval ${HOOK_SSL_CERTIFICATE_DELETED} "$sslCertificateName" || true
            fi
        fi
    done <<< "$existingSSLCertificateNames"
}

# First run
check_secret

while : ; do
    if ! sync_all; then
        echo "Sync failed"

        if [[ "$LOOP_SLEEP_SECONDS" == "once" ]]; then
            exit 1
        fi
    fi

    if [[ "$LOOP_SLEEP_SECONDS" == "once" ]]; then
        exit 0
    fi

    sleep "$LOOP_SLEEP_SECONDS"
done