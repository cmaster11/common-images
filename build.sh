#!/bin/sh
set -ex

projectDir="$1"
projectName="${2:-$projectDir}"
version=$(cat "$projectDir/VERSION")

if [ -z "$CI_REGISTRY_IMAGE" ]; then

    # Local build
    tag="$projectName:$version"
    docker build -t "$tag" "$projectDir"

else 

    # CI build
    tag="$CI_REGISTRY_IMAGE/$projectName:$version"
    docker build -t "$tag" "$projectDir"
    docker push "$tag"

fi
