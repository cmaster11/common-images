#!/bin/bash
set -e

frontendHealth="${FRONTEND_HEALTH:-health}"

echo "Disabling health endpoint"
echo "disable frontend $frontendHealth" | socat "tcp:127.0.0.1:9998" -

sleep 5
echo "Reloading"
echo "reload" | socat "tcp:127.0.0.1:9999" -

sleep 1
echo "Enabling health endpoint"
echo "disable frontend frontendHealth" | socat "tcp:127.0.0.1:9998" -